(in-package :two-quick-ones)


(defun create-servers ()
  (let* ((socket1 (usocket:socket-listen "127.0.0.1" 6005))
	 (socket2 (usocket:socket-listen "127.0.0.1" 6006))
	 (socket3 (usocket:socket-listen "127.0.0.1" 6007))
	 (connection1 (usocket:socket-accept socket1 :element-type 'character))
	 (connection2 (usocket:socket-accept socket2 :element-type 'character))
	 (connection3 (usocket:socket-accept socket3 :element-type 'character)))
    (unwind-protect
         (progn
	   (princ "made it here")
	   (format (usocket:socket-stream connection1) "import numpy")
	   (force-output (usocket:socket-stream connection1)))
      (progn
	    (format t "Closing sockets~%")
	    (usocket:socket-close connection1)
	    (usocket:socket-close connection2)
	    (usocket:socket-close connection3)
            (usocket:socket-close socket1)
	    (usocket:socket-close socket2)
	    (usocket:socket-close socket3)))))

;;(create-server 6020)
;;(create-servers)

;; (pystart "C:/UPBGE/blenderplayer.exe")

;; (pyversion-info)

;; *python*
;; (python-alive-p)

;; (uiop:process-alive-p *python*)

;; (setq *python*
;;                     #+(or os-windows windows)
;;                     (uiop:launch-program
;;                      (concatenate 'string
;;                                   "set OLDPYTHONIOENCODING=PYTHONIOENCODING && set PYTHONIOENCODING=utf8 && "
;;                                   "c:/UPBGE/blenderplayer.exe"
;;                                   " c:/UPBGE/test.blend"
;;                                   " & set PYTHONIOENCODING=OLDPYTHONIOENCODING")
;;                      :input :stream
;;                      :output :stream
;;                      :error-output :stream))
