(in-package :py4cl2)

(defun new-pystart (&optional (command (config-var 'pycmd)))
  "Start a new python subprocess
This sets the global variable *python* to the process handle,
in addition to returning it.
COMMAND is a string with the python executable to launch e.g. \"python\"
By default this is is set to *PYTHON-COMMAND*
"
  (progn (princ "bro")
	 (let ((stdin-socket (usocket:socket-listen "127.0.0.1" 6005))
	       (stdout-socket (usocket:socket-listen "127.0.0.1" 6006))
	       (stderr-socket (usocket:socket-listen "127.0.0.1" 6007))
	       (stdin-connection nil)
	       (stdout-connection nil)
	       (stderr-connection nil))
	   (princ "it's probably freezing there")
	   (unwind-protect 
		(progn (flet ((bash-escape-string (string) ; TODO: Better way to do things!
				;; We want strings such as
				;; "/user/ram-disk/test (hello''/miniconda(3')/bin/"
				;; to be escaped correctly.
				;; This function only exists in the context of PYSTART
				(with-output-to-string (*standard-output*)
				  (iter (for ch in-string string)
				    (case ch
				      (#\' (write-string "\\'"))
				      (#\( (write-string "\\("))
				      (#\) (write-string "\\)"))
				      (#\space (write-string "\\ "))
				      (t (write-char ch)))))))
			 (declare (ignorable (function bash-escape-string)))
			 (princ "made it here")
			 (loop :until (python-alive-p)
			       :do (progn (setq *python*
						#+(or os-windows windows)
						(uiop:launch-program
						 (concatenate 'string
							      "set OLDPYTHONIOENCODING=PYTHONIOENCODING && set PYTHONIOENCODING=utf8 && "
							      command
							      " c:/UPBGE/test.blend"
							      " -"
							      " c:/UPBGE/"
							      " & set PYTHONIOENCODING=OLDPYTHONIOENCODING")
						 :input :stream
						 :output :stream
						 :error-output :stream)
						#+unix
						(uiop:launch-program
						 (concatenate 'string
							      "bash -c \""
							      (bash-escape-string command)
							      " -u "
							      ;; Unbuffered is important if flush=True
							      ;; should not be required for asynchronous output.
							      ;; TODO: Add test for unflushed async output; been unable to
							      ;; The closest thing is the INTERRUPT test.
							      "\"' <(cat <<\"EOF\""
							      (string #\newline)
							      *python-code*
							      (string #\newline)
							      "EOF"
							      (string #\newline)
							      ")'\" "
							      (bash-escape-string
							       (directory-namestring
								(asdf:component-pathname
								 (asdf:find-component
								  :py4cl2 "python-code"))))
							      "\"")
						 :input :stream
						 :output :stream
						 :error-output :stream))
					  
					  (princ "looping")
					  (setf ;;stdin-socket (usocket:socket-listen "127.0.0.1" 6005)
					   ;;stdout-socket (usocket:socket-listen "127.0.0.1" 6006)
					   ;;stderr-socket (usocket:socket-listen "127.0.0.1" 6007)
					   stdin-connection (usocket:socket-accept stdin-socket :element-type 'character)
					   stdout-connection (usocket:socket-accept stdout-socket :element-type 'character)
					   stderr-connection (usocket:socket-accept stderr-socket :element-type 'character))
					  (princ "test1")
					  (let* ((stdin-stream (usocket:socket-stream stdin-connection))
						 (stdout-stream (usocket:socket-stream stdout-connection))
						 (stderr-stream (usocket:socket-stream stderr-connection)))
					    (defun force-close-sockets ()
					      (format t "Closing sockets~%")
					      (usocket:socket-close stderr-connection)
					      (usocket:socket-close stderr-socket)
					      (usocket:socket-close stdout-connection)
					      (usocket:socket-close stdout-socket)
					      (usocket:socket-close stdin-connection)
					      (usocket:socket-close stdin-socket))
					    (setf (slot-value *python* 'UIOP/LAUNCH-PROGRAM::input-stream) stdin-stream
						  (slot-value *python* 'UIOP/LAUNCH-PROGRAM::output-stream) stdout-stream
						  (slot-value *python* 'UIOP/LAUNCH-PROGRAM::error-output-stream) stderr-stream)
					    (princ (equalp stdin-stream (slot-value *python* 'UIOP/LAUNCH-PROGRAM::input-stream)))
					    (princ (equalp stdin-stream (slot-value *python* 'UIOP/LAUNCH-PROGRAM::output-stream)))))
				   (unless (python-alive-p)
				     (let ((*python-startup-error* (or (ignore-errors
									(read-stream-content-into-string
									 (uiop:process-info-error-output *python*)))
								       "Unable to fetch more error details on ECL")))
				       (cerror "Provide another path (setf (config-var 'pycmd) ...)"
					       'python-process-startup-error :command command))
				     (format t "~&Provide the path to python binary to use (eg python): ")
				     (let ((cmd (read-line)))
				       (setf (config-var 'pycmd) cmd)
				       (setf command cmd)))))
		       (unless *py4cl-tests*
			 (setq *python-output-thread*
			       (bt:make-thread
				(lambda ()
				  (when *python*
				    (let ((py-out (uiop:process-info-error-output *python*)))
				      (iter outer
					(while (and *python* (python-alive-p *python*)))
					(handler-case
					    (for char =
						 (progn
						   ;; PEEK-CHAR waits for input
						   (peek-char nil py-out nil)
						   (when *in-with-python-output*
						     (iter (while *in-with-python-output*)
						       (bt:wait-on-semaphore *python-output-semaphore*))
						     (in outer (next-iteration)))
						   (read-char py-out nil)))
					  (simple-error (condition)
					    (unless (and (member :ccl *features*)
							 (search "is private to" (format nil "~A" condition)))
					      (error "~S~%  ~A~%occured while inside *python-output-thread* ~A"
						     condition condition *python-output-thread*)))
					  (stream-error (condition)
					    (unless (member :abcl *features*)
					      (error "~S~%  ~A~%occured while inside *python-output-thread* ~A"
						     condition condition *python-output-thread*))))
					(when char (write-char char)))))))))
		       (cond ((and (numpy-installed-p)
				   (not (member :arrays *internal-features*)))
			      (push :arrays *internal-features*))
			     ((and (not (numpy-installed-p))
				   (member :arrays *internal-features*))
			      (removef *internal-features* :arrays)))
		       (incf *current-python-process-id*))
	     (progn
	       (format t "Closing sockets~%")
	       (usocket:socket-close stderr-connection)
	       (usocket:socket-close stderr-socket)
	       (usocket:socket-close stdout-connection)
	       (usocket:socket-close stdout-socket)
	       (usocket:socket-close stdin-connection)
	       (usocket:socket-close stdin-socket))))))

;; (py4cl2::pyversion-info)
;; (describe *python*)
;; (princ (equalp (slot-value *python* 'UIOP/LAUNCH-PROGRAM::input-stream) nil))
;; (progn
;;   (clear-output stdin-stream)
;;   (clear-output stdout-stream)
;;   (clear-output stderr-stream)
;;   (py4cl2:pyeval "1 + 1"))
;; (py4cl2::force-close-sockets)
;; (python-alive-p)
;; (in-package :two-quick-ones)
;; (py4cl2::pystart)
;; (py4cl2::new-pystart "C:/UPBGE/blenderplayer.exe")
;; (py4cl2::new-pystart "python")
;;(removef *internal-features* :typed-arrays)
;;(removef *internal-features* :fast-large-array-transfer)
;;(numpy-installed-p)




