(defsystem "two-quick-ones"
  :version "0.2.0"
  :author "Jason Chandler"
  :license "GPL-3"
  :depends-on (:py4cl2 :usocket) 
  :components ((:module "src"
                :components
			((:file "package-def")
			 (:file "redirect-sockets")
			 (:file "main"))))
  :description "Experimental game with UPBGE")

